import json
from datetime import datetime
from io import BytesIO
from typing import Optional
from zipfile import ZipFile

import requests
from flask import Blueprint, render_template, request, send_file
from pydantic import BaseModel

from src.schemas import Config, Template

main_page = Blueprint('main_page', __name__, template_folder='templates')


def init_config() -> Config:
    with open("templates/config.json", "r") as config_file:
        return Config(**json.loads(config_file.read()))


@main_page.route("/")
def flask_main():
    templates = init_config()
    return render_template('banner_maker.html', templates_json=templates.json(), templates_config=templates)


class FormData(BaseModel):
    name: Optional[str]

    @property
    def selected_template(self) -> Template:
        for template in init_config().templates:
            if template.name == self.name:
                return template


def parse_form_data(raw_form) -> FormData:
    return FormData(name=raw_form.get('select_template'))


@main_page.route("/form", methods=["POST"])
def receive_form():
    form_data = parse_form_data(request.form)

    stream = BytesIO()

    files = request.files.getlist("background_image")

    with ZipFile(stream, 'w') as zf:
        for file in files:
            files = {"background_image": (
                'background_image.png', BytesIO(file.stream.read()), 'image/png')}

            template = form_data.selected_template
            logo = requests.get(template.logo_url)

            files["logo"] = ('logo.png', BytesIO(logo.content), 'image/png')

            params = dict(
                position=f"{template.relative_position_vertical.upper()}_{template.relative_position_horizontal.upper()}")
            response = requests.post("http://localhost:8000/api/generate/image", files=files, params=params)

            response.raise_for_status()
            zf.writestr(file.filename, response.content)
    stream.seek(0)

    return send_file(stream, as_attachment=True, download_name=f'{datetime.utcnow().isoformat().replace(":", "_")}watermark.zip')
