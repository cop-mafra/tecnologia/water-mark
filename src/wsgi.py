from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.wsgi import WSGIMiddleware
from flask import Flask

from src.api import api
from src.frontend import main_page

load_dotenv()

flask_app = Flask(__name__)

flask_app.register_blueprint(main_page)

app = FastAPI(title="Water Marker API", docs_url="/api/docs", openapi_url="/api/openapi.json")

app.include_router(api)
app.mount("/", WSGIMiddleware(flask_app))
