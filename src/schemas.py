from enum import Enum
from typing import List

from pydantic import BaseModel


class Template(BaseModel):
    name: str
    relative_position_horizontal: str
    relative_position_vertical: str
    logo_url: str


class Config(BaseModel):
    templates: List[Template]


class Positions(tuple, Enum):
    BOTTOM_LEFT = (60, -40)
    BOTTOM_RIGHT = (-60, -40)
    TOP_LEFT = (60, 40)
    TOP_RIGHT = (-60, 40)


POSITIONS_DICT = {position.name: position for position in Positions}
