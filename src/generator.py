from io import BytesIO
from typing import BinaryIO

from PIL import Image
from fastapi import UploadFile

from src.schemas import Positions


def paste_logo(original_image: Image, position: Positions, image_to_paste: BinaryIO):
    original_width, original_height = original_image.size
    target_x, target_y = position.value

    x_inverted = target_x < 0
    y_inverted = target_y < 0

    output_x = target_x if not x_inverted else original_width + target_x
    output_y = target_y if not y_inverted else original_height + target_y

    print((output_x, output_y))

    image_to_paste = Image.open(image_to_paste).convert("RGBA")

    image_to_paste_width, image_to_paste_height = image_to_paste.size
    image_to_paste_aspect_ratio = image_to_paste_width / image_to_paste_height
    target_width = int(original_width / 5)
    target_height = int(target_width / image_to_paste_aspect_ratio)

    image_to_paste = image_to_paste.resize((target_width, target_height))

    coords_to_paste = (
        output_x - target_width if x_inverted else output_x, output_y - target_height if y_inverted else output_y)
    original_image.paste(image_to_paste, coords_to_paste, image_to_paste)


def generate_image(background_image: UploadFile, logo: UploadFile, position: Positions) -> BytesIO:
    image = Image.open(background_image.file).convert("RGBA")

    paste_logo(image, position, logo.file)

    output = BytesIO()
    image.save(output, "PNG")
    output.seek(0)

    return output
