from fastapi import APIRouter, UploadFile
from starlette.responses import StreamingResponse

from src.generator import generate_image
from src.schemas import POSITIONS_DICT

api = APIRouter(prefix="/api")


@api.post("/generate/image", response_class=StreamingResponse)
def post_generate_image(background_image: UploadFile, position: str, logo: UploadFile):
    return StreamingResponse(
        content=generate_image(background_image=background_image, logo=logo, position=POSITIONS_DICT.get(position)),
        media_type="image/png")
