# Water Mark

### Problem
The need to place the logo image in every public media content

### Solution
Webpage that creates the image with the required logo

### UI
![watermarkui](img/watermarkui.png)

#### Description
1. Template Selector
2. Browse device file system
3. Generate Button

### Features
Multiple files can be sent

### Templates
Describe the place for the logo

Are defined on `./templates`

### Output
It will download on browser and will be named as {datetime.utcnow.iso}_watermark.zip

### Contact
Any issue contact [Rodrigo Simões](mailto:rodrigofrnsimoes@outlook.com)